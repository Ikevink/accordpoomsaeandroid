package com.poomsae.accord.accordpoomsaeandroid;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by Ikevink on 16/11/2014.
 */
public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
