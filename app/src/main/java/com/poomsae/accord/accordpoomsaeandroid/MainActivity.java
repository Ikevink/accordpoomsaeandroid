package com.poomsae.accord.accordpoomsaeandroid;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.kikudjiro.android.net.INetClientDelegate;
import com.kikudjiro.android.net.NetClient;
import com.kikudjiro.android.net.NetClient.State;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity implements INetClientDelegate {
    public final DecimalFormat formatter = new DecimalFormat("#.#");
    protected NetClient client;
    private final short Task_Lock = 1;
    private final short Task_Unlock = 0;
    private final short Task_Query = 2;

    String phaseid = "";

    float power = 2.0f;
    float rhythm = 2.0f;
    float energy = 2.0f;

    //float presentation = 0;
    //float accuracy = 4.0f;

    int numMajorDeductions = 0;
    int numMinorDeductions = 0;

    short currentTask = -1;
    //boolean finished = false;

    Timer timer;
    TimerTask timerTask;

    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 5000, 10000); //
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        currentTask = Task_Query;
                        connect();
                    }
                });
            }
        };
    }

    public void query() {
        new SendTask(client).execute("query");
    }

    public class ConnectTask extends AsyncTask<NetClient, Void, Void> {
        String host;
        int port;
        ConnectTask(String host, int port) {
            this.host = host;
            this.port = port;
        }
        @Override
        protected Void doInBackground(NetClient... params) {
            params[0].connect(host, port);

            return null;
        }
    }

    public class SendTask extends AsyncTask<String, Void, Void> {
        NetClient client;
        public SendTask(NetClient client) {
            this.client = client;

        }

        @Override
        protected Void doInBackground(String... params) {
            String[] arrayedParams = new String[params.length];
            for (int i = 0; i < params.length; i++) {
                arrayedParams[i] = params[i];
            }

            switch (arrayedParams.length) {
                case 1: client.send(arrayedParams[0]);
                        break;

                case 2: client.send(arrayedParams[0], arrayedParams[1]);
                        break;

                case 3: client.send(arrayedParams[0], arrayedParams[1], arrayedParams[2]);
                        break;

                case 4: client.send(arrayedParams[0], arrayedParams[1], arrayedParams[2], arrayedParams[3]);
                        break;

                case 5: client.send(arrayedParams[0], arrayedParams[1], arrayedParams[2], arrayedParams[3], arrayedParams[4]);
                    break;

                default:
                        toast("invalid number params " + arrayedParams.length);
                        break;
            }

            return null;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout lay = (LinearLayout)findViewById(R.id.layPresentation);


        GenericDull((LinearLayout)findViewById(R.id.txtAccuracy).getParent(), true);
        GenericDull((LinearLayout)findViewById(R.id.txtPresentation).getParent(), true);
        GenericDull((LinearLayout) findViewById(R.id.txtTotal).getParent(), true);
        AccuracyDull(true);
        PresentationDull(true);

        //connect();
        startTimer();

    }

    public void powerChanged(View v) {
        // do something when the button is clicked

        LinearLayout lay = (LinearLayout)v.getParent();
        for(int i=0; i<lay.getChildCount(); ++i) {
            View nextChild = lay.getChildAt(i);
            nextChild.setBackgroundResource(R.drawable.presentation_selector_button);
        }

        v.setBackgroundResource(R.drawable.presentation_selector_selected_button);

        //Button lock = (Button)findViewById(R.id.btnLock);

        switch (lay.getId()) {
            case R.id.tblPower:
                power = Float.parseFloat(((Button)v).getText().toString());
                break;
            case R.id.tblEnergy:
                energy = Float.parseFloat(((Button)v).getText().toString());
                break;
            case R.id.tblRhythm:
                rhythm = Float.parseFloat(((Button)v).getText().toString());
                break;
            default:
                //lock.setText("got unknown");
                break;
        }
        UpdateGUI();
    }



    private void PresentationDull(boolean shouldDull) {
        LinearLayout layPower = (LinearLayout)findViewById(R.id.tblPower);
        LinearLayout layRhythm = (LinearLayout)findViewById(R.id.tblRhythm);
        LinearLayout layEnergy = (LinearLayout)findViewById(R.id.tblEnergy);

        for(int i=0; i<layPower.getChildCount(); ++i) {
            Button powerButton = (Button)layPower.getChildAt(i);
            Button energyButton = (Button)layEnergy.getChildAt(i);
            Button rhythmButton = (Button)layRhythm.getChildAt(i);

            if (shouldDull) {
                powerButton.setClickable(false);
                energyButton.setClickable(false);
                rhythmButton.setClickable(false);
                if (powerButton.getText().equals(formatter.format(power))) {
                    powerButton.setBackgroundResource(R.color.Dull_Aqua);
                    powerButton.setTextColor(getResources().getColor(R.color.Dull_Gray));
                } else {
                    powerButton.setBackgroundResource(R.color.Dull_Gray);
                    powerButton.setTextColor(getResources().getColor(R.color.Dull_Gray));
                }
                if (energyButton.getText().equals(formatter.format(energy))) {
                    energyButton.setBackgroundResource(R.color.Dull_Aqua);
                    energyButton.setTextColor(getResources().getColor(R.color.Dull_Gray));
                } else {
                    energyButton.setBackgroundResource(R.color.Dull_Gray);
                    energyButton.setTextColor(getResources().getColor(R.color.Dull_Gray));
                }
                if (rhythmButton.getText().equals(formatter.format(rhythm))) {
                    rhythmButton.setBackgroundResource(R.color.Dull_Aqua);
                    rhythmButton.setTextColor(getResources().getColor(R.color.Dull_Gray));
                } else {
                    rhythmButton.setBackgroundResource(R.color.Dull_Gray);
                    rhythmButton.setTextColor(getResources().getColor(R.color.Dull_Gray));
                }

            } else {
                powerButton.setClickable(true);
                energyButton.setClickable(true);
                rhythmButton.setClickable(true);
                if (powerButton.getText() == formatter.format(power)) {
                    powerButton.setBackgroundResource(R.drawable.presentation_selector_selected_button);
                    powerButton.setTextColor(getResources().getColor(R.color.Black));
                } else {
                    powerButton.setBackgroundResource(R.drawable.presentation_selector_button);
                    powerButton.setTextColor(getResources().getColor(R.color.Black));
                }
                if (energyButton.getText().equals(formatter.format(energy))) {
                    energyButton.setBackgroundResource(R.drawable.presentation_selector_selected_button);
                    energyButton.setTextColor(getResources().getColor(R.color.Black));
                } else {
                    energyButton.setBackgroundResource(R.drawable.presentation_selector_button);
                    energyButton.setTextColor(getResources().getColor(R.color.Black));
                }
                if (rhythmButton.getText().equals(formatter.format(rhythm))) {
                    rhythmButton.setBackgroundResource(R.drawable.presentation_selector_selected_button);
                    rhythmButton.setTextColor(getResources().getColor(R.color.Black));
                } else {
                    rhythmButton.setBackgroundResource(R.drawable.presentation_selector_button);
                    rhythmButton.setTextColor(getResources().getColor(R.color.Black));
                }
            }
        }
    }

    private void AccuracyDull(boolean shouldDull) {
        if (shouldDull) {
            findViewById(R.id.btnMajorDeduction).setBackgroundResource(R.color.Dull_Orange);
            findViewById(R.id.btnMajorDeduction).setClickable(false);

            findViewById(R.id.btnUndoMajor).setBackgroundResource(R.color.Dull_Green);
            findViewById(R.id.btnUndoMajor).setClickable(false);

            findViewById(R.id.btnMinorDeduction).setBackgroundResource(R.color.Dull_Orange);
            findViewById(R.id.btnMinorDeduction).setClickable(false);

            findViewById(R.id.btnUndoMinor).setBackgroundResource(R.color.Dull_Green);
            findViewById(R.id.btnUndoMinor).setClickable(false);
        } else {
            findViewById(R.id.btnMajorDeduction).setBackgroundResource(R.color.Orange);
            findViewById(R.id.btnMajorDeduction).setClickable(true);
            findViewById(R.id.btnUndoMajor).setBackgroundResource(R.color.Green);
            findViewById(R.id.btnUndoMajor).setClickable(true);
            findViewById(R.id.btnMinorDeduction).setBackgroundResource(R.color.Orange);
            findViewById(R.id.btnMinorDeduction).setClickable(true);
            findViewById(R.id.btnUndoMinor).setBackgroundResource(R.color.Green);
            findViewById(R.id.btnUndoMinor).setClickable(true);

        }
    }

    private void GenericDull(LinearLayout lay, boolean shouldDull) {
        for(int i=0; i<lay.getChildCount(); ++i) {
            View nextChild = lay.getChildAt(i);
            if (nextChild.getClass() == TextView.class &&
                    (((TextView) nextChild).getText().equals("Accuracy") ||
                            ((TextView) nextChild).getText().equals("Presentation") ||
                            ((TextView) nextChild).getText().equals("Total"))) {

            } else {
                if (nextChild.getClass() == LinearLayout.class) {
                    for (int j = 0; j < ((LinearLayout) nextChild).getChildCount(); ++j) {
                        View innerChild = ((LinearLayout) nextChild).getChildAt(j);
                        if (shouldDull) {
                            innerChild.setBackgroundResource(R.color.Dull_Blue);
                        } else {
                            innerChild.setBackgroundResource(R.color.Dark_Blue);
                        }

                    }


                }
                if (shouldDull) {
                    if (nextChild.getClass() == TextView.class) {
                        try {
                            Float.parseFloat(((TextView) nextChild).getText().toString());
                            ((TextView) nextChild).setTextColor(getResources().getColor(R.color.Dull_White));
                        } catch (Exception e) {
                            // don't do anything
                        }

                    }
                    nextChild.setBackgroundResource(R.color.Dull_Blue);
                } else {
                    if (nextChild.getClass() == TextView.class) {
                        try {
                            Float.parseFloat(((TextView) nextChild).getText().toString());
                            ((TextView) nextChild).setTextColor(getResources().getColor(R.color.White));
                        } catch (Exception e) {
                            // don't do anything
                        }

                    }
                    nextChild.setBackgroundResource(R.color.Dark_Blue);
                }
            }
        }
    }

    public void btnAccuracy_clicked(View v) {
        GenericDull((LinearLayout) findViewById(R.id.txtAccuracy).getParent(), false);
        GenericDull((LinearLayout)findViewById(R.id.txtPresentation).getParent(), true);
        GenericDull((LinearLayout)findViewById(R.id.txtTotal).getParent(), true);
        //DullAccuracy(false);
        //DullPresentation(true);
        //DullTotal(true);
        //finished = false;
        currentTask = Task_Unlock;


        AccuracyDull(false);
        PresentationDull(true);

        connect();
    }

    public void btnPresentation_clicked(View v) {
        GenericDull((LinearLayout)findViewById(R.id.txtAccuracy).getParent(), true);
        GenericDull((LinearLayout)findViewById(R.id.txtPresentation).getParent(), false);
        GenericDull((LinearLayout)findViewById(R.id.txtTotal).getParent(), true);
        //DullAccuracy(true);
        //DullPresentation(false);
        //DullTotal(true);
        //finished = false;
        currentTask = Task_Unlock;

        AccuracyDull(true);
        PresentationDull(false);



        connect();
    }

    public void btnTotal_clicked(View v) {
        GenericDull((LinearLayout)findViewById(R.id.txtAccuracy).getParent(), true);
        GenericDull((LinearLayout)findViewById(R.id.txtPresentation).getParent(), true);
        GenericDull((LinearLayout)findViewById(R.id.txtTotal).getParent(), false);
        //DullAccuracy(true);
        //DullPresentation(true);
        //DullTotal(false);
        //finished = true;
        currentTask = Task_Lock;

        AccuracyDull(true);
        PresentationDull(true);

        connect();

    }

    private float getTechnical() {
        int intAccuracy = 40 - 3 * numMajorDeductions - numMinorDeductions;     // multiplied numbers by 10
        return intAccuracy / 10.0f;
    }

    private float getPresentation() {
        return power + rhythm + energy;
    }

    private void UpdateGUI() {
        String techTotal = formatter.format(getTechnical());

        TextView txtAccuracy = (TextView) findViewById(R.id.txtAccuracy);
        txtAccuracy.setText(techTotal);

        TextView txtMajorDeductions = (TextView) findViewById(R.id.txtMajorDeductions);
        txtMajorDeductions.setText("0.3's: (" + numMajorDeductions + ")");
        TextView txtMinorDeductions = (TextView) findViewById(R.id.txtMinorDeductions);
        txtMinorDeductions.setText("0.1's: (" + numMinorDeductions + ")");

        //////////////////////////////////////////////////////////////////////////////////

        String presTotal = formatter.format(getPresentation());

        TextView txtPresentation = (TextView) findViewById(R.id.txtPresentation);
        txtPresentation.setText(presTotal);

        //////////////////////////////////////////////////////////////////////////////////

        String total = formatter.format(getTechnical() + getPresentation());
        TextView txtTotal = (TextView) findViewById(R.id.txtTotal);
        txtTotal.setText(/*"\n" + */total);
    }



    private void lock() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String username = prefs.getString("prefName", "Bob");
        String pass = prefs.getString("prefKey", "Judge1");

        new SendTask(client).execute("lock", username, pass, formatter.format(getTechnical()), formatter.format(getPresentation()));

        if (timer == null) {
            startTimer();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            //SettingsActivity
            toast("disconnected");
            //disconnect();
            Intent i = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void undoMajor_onClick(View v) {
        numMajorDeductions -= 1;
        UpdateGUI();
    }

    public void undoMinor_onClick(View v) {
        numMinorDeductions -= 1;
        UpdateGUI();
    }

    public void majorDeduction_onClick(View v) {
        numMajorDeductions += 1;
        UpdateGUI();
    }

    public void minorDeduction_onClick(View v) {
        numMinorDeductions += 1;
        UpdateGUI();
    }

    final MainActivity me = this;

    //private Handler handler;

    public void connect() {
        if (this.client != null && this.client.getState() == State.ONLINE) return;

        try {
            //SharedPreferences prefs = getSharedPreferences("com.poomsae.accord.accordpoomsaeandroid.SettingsActivity", MODE_PRIVATE);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            String host = prefs.getString("prefHost", "192.168.0.100");

            int port = Integer.parseInt(prefs.getString("prefPort", "3016").toString());

            //findViewById(R.id.btnConnect).setVisibility(View.GONE);
            //this.connected.setVisibility(View.GONE);
            //this.disconnected.setVisibility(View.GONE);

            me.client = new NetClient(me);

            new ConnectTask(host, port).execute(me.client);//.connect(host, port);
            //lock.setText("trying to connect");
        } catch (Exception e) {
            //lock.setText("stack trace");
            e.printStackTrace();
        }
    }

    @Override
    public void onConnect() {
        this.toast("connect");

        if (currentTask == Task_Lock) {
            lock();
        } else if (currentTask == Task_Unlock) {
            unlock();
        } else if (currentTask == Task_Query) {
            query();
        }

        //reset();
    }



    @Override
    public void onConnectTimeout() {
    }

    @Override
    public void onDisconnect() {
        client = null;
        //this.toast("disconnected");
    }

    private void reset() {
        LinearLayout layPower = (LinearLayout)findViewById(R.id.tblPower);
        LinearLayout layEnergy = (LinearLayout)findViewById(R.id.tblEnergy);
        LinearLayout layRhythm = (LinearLayout)findViewById(R.id.tblRhythm);

        //energy = 0;

        energy = 2.0f;
        rhythm = 2.0f;
        power =  2.0f;

        for(int i=0; i<layEnergy.getChildCount(); ++i) {
            Button energyChild = (Button) layEnergy.getChildAt(i);
            if (energyChild.getText().toString().equals("2.0")) {
                energyChild.setBackgroundResource(R.drawable.presentation_selector_selected_button);
            } else {
                energyChild.setBackgroundResource(R.drawable.presentation_selector_button);
            }
            Button rhythmChild = (Button) layRhythm.getChildAt(i);
            if (rhythmChild.getText().toString().equals("2.0")) {
                rhythmChild.setBackgroundResource(R.drawable.presentation_selector_selected_button);
            } else {
                rhythmChild.setBackgroundResource(R.drawable.presentation_selector_button);
            }
            Button powerChild = (Button) layPower.getChildAt(i);
            if (powerChild.getText().toString().equals("2.0")) {
                powerChild.setBackgroundResource(R.drawable.presentation_selector_selected_button);
            } else {
                powerChild.setBackgroundResource(R.drawable.presentation_selector_button);
            }
            //currentTask = Task_Unlock;
            ((TextView)findViewById(R.id.txtTotal)).setText("10.0");
        }

        btnAccuracy_clicked(findViewById(R.id.btnAccuracy));

        numMinorDeductions = 0;
        numMajorDeductions = 0;

        UpdateGUI();

    }

    @Override
    public void onReceive(String[] message) {
        //this.toast("receive");

        // maybe we can pass in information like the idiot's name I don't know
        /*if (message.length >= 2 && message[0].startsWith("new")) {
            reset();

            this.toast("New Competitor!\n" + message[1]);
        } else if (message.length >= 2 && message[0].startsWith("rejected")) {
            this.toast("Connection Rejected!\n" + message[1]);
        }*/
        if (message.length >= 2 && message[0].startsWith("phase")) {
            if (phaseid.compareTo(message[1]) != 0) {
                phaseid = message[1];
                stoptimertask();
                reset();
            }
        }

    }

    @Override
    public void onError(String message) {
        //this.toast(message);
        this.toast("Error: " + message);
    }

    private void toast(final String message) {
        final Activity activity = this;
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void unlock() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String username = prefs.getString("prefName", "Bob");
        String pass = prefs.getString("prefKey", "Judge1");

        new SendTask(client).execute("unlock", username, pass);

        stoptimertask();
    }
}