package com.poomsae.accord.accordpoomsaeandroid;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by Ikevink on 17/11/2014.
 */
public class SettingsActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        getActionBar().setDisplayHomeAsUpEnabled(true);
    }
}